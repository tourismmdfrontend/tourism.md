import com.example.tourismmd.dao.UserRepo;
import com.example.tourismmd.model.User;
import com.example.tourismmd.service.UserService;
import com.example.tourismmd.service.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class UserServiceImplIntegrationTest
{
    @Autowired
    private UserService userService;

    @MockBean
    private UserRepo userRepo;

    @TestConfiguration
    static class UserServiceImplTestContextConfiguration {
        @Bean
        public UserService userService() {
            return new UserServiceImpl();
        }
    }

    @Before
    public void setUp() {
        User alex = new User(13, "alex");
        Mockito.when(userRepo.findByUserName(alex.getUserName()))
                .thenReturn(alex);
    }

    @Test
    public void whenValidName_thenUserShouldBeFound()
    {
        User found = userService.getUserByUserName("alex");
        assertThat(found.getUserName())
                .isEqualTo("alex");
    }
}