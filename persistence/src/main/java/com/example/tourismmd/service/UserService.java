package com.example.tourismmd.service;

import com.example.tourismmd.model.User;

import java.util.List;

public interface UserService {

    User getUserByUserName(String name);
    List<User> getAllUsers();
}