package com.example.tourismmd.service;

import com.example.tourismmd.dao.UserRepo;
import com.example.tourismmd.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService
{
    @Autowired

    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }
    private UserRepo userRepo;

    @Override
    public User getUserByUserName(String name) {
        return userRepo.findByUserName(name);
    }

    @Override
    public List<User> getAllUsers() {
        return (List<User>) userRepo.findAll();
    }
}