package com.example.tourismmd.dao;

import com.example.tourismmd.model.Poi;
import org.springframework.data.repository.CrudRepository;

public interface PoiRepo extends CrudRepository<Poi, Integer>
{
}