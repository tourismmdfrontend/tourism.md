package com.example.tourismmd.dao;

import com.example.tourismmd.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepo extends CrudRepository<User, Integer> {
    User findByUserName(String username);
}
