package com.example.tourismmd.model;

import javax.persistence.*;
import java.util.*;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    //@NotNull
    private int Id;
    //@NotNull
    private String userName;
    //@NotNull
    private String Password;
    //@NotNull
    //@Email
    private String Email;

    public User() {
        Authorities = new ArrayList<>();
    }

    public User(int id, String name) {
        this.Id = id;
        this.userName = name;
        Authorities = new ArrayList<>();
    }

    public int getId()
    {
        return Id;
    }

    public void setId(int id)
    {
        Id = id;
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getPassword()
    {
        return Password;
    }

    public void setPassword(String password)
    {
        Password = password;
    }

    public String getEmail()
    {
        return Email;
    }

    public void setEmail(String email)
    {
        Email = email;
    }


    @Override
    public String toString()
    {
        return "User{" +
                "Id=" + Id +
                ", userName='" + userName + '\'' +
                ", Password='" + Password + '\'' +
                ", Email='" + Email + '\'' +
                ", Authorities='" + Authorities + '\'' +
                '}';
    }

    private ArrayList<AuthorityType> Authorities;


    public ArrayList<AuthorityType> getAuthorities() {
        return Authorities;
    }

    public void setAuthorities(ArrayList<AuthorityType> authorities) {
        this.Authorities = authorities;
    }
}
