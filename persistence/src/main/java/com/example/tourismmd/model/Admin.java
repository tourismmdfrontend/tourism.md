package com.example.tourismmd.model;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
public class Admin
{
    @Id
    @NotNull
    private int Id;
    @NotNull
    private String Password;
    @NotNull
    @Email
    private String Email;

    public Admin() {}
    public Admin(int id, String password, String email)
    {
        Id = id;
        Password = password;
        Email = email;
    }

    public int getId()
    {
        return Id;
    }

    public void setId(int id)
    {
        Id = id;
    }

    public String getPassword()
    {
        return Password;
    }

    public void setPassword(String password)
    {
        Password = password;
    }

    public String getEmail()
    {
        return Email;
    }

    public void setEmail(String email)
    {
        Email = email;
    }

    @Override
    public String toString()
    {
        return "Admin{" +
                "Id=" + Id +
                ", Password='" + Password + '\'' +
                ", Email='" + Email + '\'' +
                '}';
    }
}
