package com.example.tourismmd.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Poi
{
    @Id
    @NotNull
    private int Id;
    @NotNull
    private String Name;
    private String Price;
    @NotNull
    private String Description;
    @NotNull
    private String Address;
    @NotNull
    @Column(name = "image", length = 10000000)
    private String Image;
    @NotNull
    private String Rating;

    private String workHours;
    @NotNull
    private float LocationX;
    @NotNull
    private  float LocationY;
    @NotNull
    private String Type;
    @NotNull
    private String workDays;
    public Poi() {
    }

    public Poi(@NotNull int id, @NotNull String name, String price, @NotNull String description, @NotNull String address, @NotNull String image, @NotNull String rating, String workHours, @NotNull float locationX, @NotNull float locationY, @NotNull String type, @NotNull String workDays) {
        Id = id;
        Name = name;
        Price = price;
        Description = description;
        Address = address;
        Image = image;
        Rating = rating;
        this.workHours = workHours;
        LocationX = locationX;
        LocationY = locationY;
        Type = type;
        this.workDays = workDays;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return "Poi{" +
                "Id=" + Id +
                ", Name='" + Name + '\'' +
                ", Price='" + Price + '\'' +
                ", Description='" + Description + '\'' +
                ", Address='" + Address + '\'' +
                ", Image='" + Image + '\'' +
                ", Rating='" + Rating + '\'' +
                ", workHours='" + workHours + '\'' +
                ", LocationX=" + LocationX +
                ", LocationY=" + LocationY +
                ", Type='" + Type + '\'' +
                ", workDays='" + workDays + '\'' +
                '}';
    }

    public String getPrice()
    {
        return Price;
    }

    public void setPrice(String price)
    {
        Price = price;
    }

    public String getDescription()
    {
        return Description;
    }

    public void setDescription(String description)
    {
        Description = description;
    }

    public String getAddress()
    {
        return Address;
    }

    public void setAddress(String address)
    {
        Address = address;
    }

    public String getImage()
    {
        return Image;
    }

    public void setImage(String image)
    {
        Image = image;
    }

    public String getRating()
    {
        return Rating;
    }

    public void setRating(String rating)
    {
        Rating = rating;
    }

    public String getWorkHours()
    {
        return workHours;
    }

    public void setWorkHours(String workHours)
    {
        this.workHours = workHours;
    }

    public String getType()
    {
        return Type;
    }

    public void setType(String type)
    {
        Type = type;
    }

    public String getWorkDays()
    {
        return workDays;
    }

    public void setWorkDays(String workDays)
    {
        this.workDays = workDays;
    }

    public float getLocationX() {
        return LocationX;
    }

    public void setLocationX(float locationX) {
        LocationX = locationX;
    }

    public float getLocationY() {
        return LocationY;
    }

    public void setLocationY(float locationY) {
        LocationY = locationY;
    }
}
