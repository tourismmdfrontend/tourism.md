package com.example.tourismmd.services;

import com.example.tourismmd.model.User;

public interface UserService
{
    void save(User user);

    User findByUserName(String username);

}
