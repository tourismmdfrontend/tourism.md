package com.example.tourismmd.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class MainController
{
    @RequestMapping(method = RequestMethod.GET, value = "/")
    public String mainPage() {
        return "redirect:swagger-ui.html";
    }
}