package com.example.tourismmd.controllers;

import com.example.tourismmd.dao.UserRepo;
import com.example.tourismmd.model.HTTPStatus;
import com.example.tourismmd.model.User;
import com.example.tourismmd.services.CustomUserDetails;
import com.example.tourismmd.services.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/api/user")
public class UserController
{
    private final

    UserRepo userRepo;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private AuthenticationManager authenticationManager;

    public UserController(UserRepo userRepo)
    {
        this.userRepo = userRepo;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public HTTPStatus homepage() {

        return new HTTPStatus(200, "No Error", "OK");
    }

    @RequestMapping(method = {RequestMethod.POST, RequestMethod.GET} , value = "/register")
    public HTTPStatus addUser(@RequestBody User user) {
        userRepo.save(user);
        securityService.autoLogin(user.getUserName(), user.getPassword());

        return new HTTPStatus(200, "No Error", "OK");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/login")
    public String login() {
        return "login";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/login/{username}/{password}")
    public HTTPStatus login(@PathVariable("username") String username, @PathVariable("password") String password) {
        User user = userRepo.findByUserName(username);

        if(user != null){
            if( password.equals(user.getPassword())){
//                UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
//                validatePrinciple(authentication.getPrincipal());
//                User loggedInUser = ((CustomUserDetails) authentication.getPrincipal()).getUserDetails();
                return new HTTPStatus(200, "login is ok", "OK");
            }
            return new HTTPStatus(400, "Wrong username or password", "NOT OK");
        }

        return new HTTPStatus(404, "Not found user with this username", "NOT OK");
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public HTTPStatus logout(SessionStatus session) {
        SecurityContextHolder.getContext().setAuthentication(null);
        session.setComplete();
        return new HTTPStatus(200, "logout successfully", "OK");
    }

    @RequestMapping(method = RequestMethod.GET, value = "/list")
    public List<User> getUsers() {
        return (List<User>) userRepo.findAll();
    }

    private void validatePrinciple(Object principal) {
        if (!(principal instanceof CustomUserDetails)) {
            throw new  IllegalArgumentException("Principal can not be null!");
        }
    }
}
