package com.example.tourismmd.controllers;

import com.example.tourismmd.dao.PoiRepo;
import com.example.tourismmd.model.Poi;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/api/poi")
public class PoiController
{

    private final
    PoiRepo poiRepo;

    public PoiController(PoiRepo poiRepo) {
        this.poiRepo = poiRepo;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public void mainPoi() {
    }
    @RequestMapping(method = RequestMethod.GET, value = "/list")
    public List<Poi> PoiList(){
        return (List<Poi>) poiRepo.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Poi getPoi(@PathVariable int id) {
        if(poiRepo.findById(id).isPresent())
            return poiRepo.findById(id).get();
        return null;
    }
}